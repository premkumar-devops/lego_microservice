/**
 * 
 */
package com.lego.microservices.msg;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Suresh.Rupnar
 *
 */
public class Messages {
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("com.lego.microservices.msg.message", Locale.getDefault());
	
	public Messages() {
//		bundle = ResourceBundle.getBundle("MessageBundle", Locale.US);
	}
	
	public static String getMessage(String key) {
		return bundle.getString(key);
	}
	
	public static void main(String[] args) {
		System.out.println(getMessage("errorCodeMessage_BlankUIDPSWD"));
	}

}
