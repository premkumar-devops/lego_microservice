package com.lego.microservices.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

import org.springframework.http.HttpStatus;

/**
 * Allow the controller to return a 404 if an account is not found by simply
 * throwing this exception. The @ResponseStatus causes Spring MVC to return a
 * 404 instead of the usual 500.
 * 
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String userID) {
		super(Messages.getMessage("User_Not_Found_Code") + " " + "No such user: " + userID);
	}
	public UserNotFoundException(String userID,String password) {
		super(Messages.getMessage("Invalid_UserName_Password_code") +  " " + Messages.getMessage("Invalid_UserName_Password_Message"));
	}
}
