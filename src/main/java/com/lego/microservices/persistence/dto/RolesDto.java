/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.lego.microservices.services.users.UsersController;

/**
 * Role DTO - used to interact with the {@link UsersController}.
 * @author Suresh.Rupnar
 *
 */
@JsonRootName("Role")
public class RolesDto {

	private Integer roleId;
	private String roleName;
	private String attrName;
	private String attrValue;

	/**
	 * Default constructor for JPA only.
	 */
	public RolesDto() {
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName
	 *            the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the attrName
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @param attrName
	 *            the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * @return the attrValue
	 */
	public String getAttrValue() {
		return attrValue;
	}

	/**
	 * @param attrValue
	 *            the attrValue to set
	 */
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RolesDto [roleId=" + roleId + ", roleName=" + roleName + ", attrName=" + attrName + ", attrValue="
				+ attrValue + "]";
	}

}
