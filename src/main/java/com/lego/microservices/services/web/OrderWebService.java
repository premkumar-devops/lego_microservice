package com.lego.microservices.services.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lego.microservices.exceptions.util.ExceptionHandlerResource;
import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.dto.ErrorDto;
import com.lego.microservices.persistence.dto.GenericOrderSearchDto;
import com.lego.microservices.persistence.dto.OrderCreationDto;
import com.lego.microservices.persistence.dto.OrderDto;
import com.lego.microservices.persistence.dto.OrderSummaryDto;
import com.lego.microservices.services.util.MyResponseErrorHandler;
import com.lego.microservices.services.util.RestUtil;

/**
 * Hide the access to the microservice inside this local service.
 * 
 * @author Suresh.Rupnar
 * 
 */
@Service
public class OrderWebService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	private ObjectMapper objectMapper;

	protected Logger logger = Logger.getLogger(OrderWebService.class.getName());

	/**
	 * default Constructor
	 * 
	 * @param serviceUrl
	 */
	public OrderWebService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
		objectMapper = new ObjectMapper();
	}

	/**
	 * The RestTemplate works because it uses a custom request-factory that uses
	 * Ribbon to look-up the service to use. This method simply exists to show
	 * this.
	 */
	@PostConstruct
	public void demoOnly() {
		// Can't do this in the constructor because the RestTemplate injection
		// happens afterwards.
		logger.warning("The RestTemplate request factory is " + restTemplate.getRequestFactory().getClass());
	}

	/**
	 * Fetch list of all orders
	 * 
	 * @return
	 */
	public List<Object> getAllOrders() {

		logger.info("Calling getAllOrders() Order-Web REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/orders", HttpMethod.GET, request,
				String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				ErrorDto errorDto = new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
				List<Object> errorlist = new ArrayList<Object>();
				errorlist.add(errorDto);
				return errorlist;
			} else {
				OrderSummaryDto[] orderDtoList = objectMapper.readValue(responseBody, OrderSummaryDto[].class);
				List<Object> objectList = new ArrayList<Object>();
				objectList.addAll(Arrays.asList(orderDtoList));
				return objectList;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fetch list of all orders filtered by Role Attribute name and value
	 * 
	 * @return
	 */
	public List<Object> getOrders(String roleAttrName, String roleAttrValue) {

		logger.info("Calling getOrders(String roleAttrName, String roleAttrValue) Order-Web REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(
				serviceUrl + "/orders/" + roleAttrName + "/" + roleAttrValue, HttpMethod.GET, request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				ErrorDto errorDto = new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
				List<Object> errorlist = new ArrayList<Object>();
				errorlist.add(errorDto);
				return errorlist;
			} else {
				OrderSummaryDto[] orderDtoList = objectMapper.readValue(responseBody, OrderSummaryDto[].class);
				List<Object> objectList = new ArrayList<Object>();
				objectList.addAll(Arrays.asList(orderDtoList));
				return objectList;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fetch order details by order ID
	 * 
	 * @return
	 */
	public Object getOrderById(long orderId) {

		logger.info("Calling getOrderById (long orderId) Order-Web REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/orders/" + orderId, HttpMethod.GET,
				request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				ErrorDto errorDto = new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
				List<Object> errorlist = new ArrayList<Object>();
				errorlist.add(errorDto);
				return errorlist;
			} else {
				OrderDto orderDto = objectMapper.readValue(responseBody, OrderDto.class);
				return orderDto;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fetch list of all orders basis on orderId, mouldNo and artNo
	 * 
	 * @return List<Object>, if order found. Else error message
	 */
	public List<Object> getGenericSearch(String genericAttribute) {

		logger.info("Calling getGenericSearch (String genericAttribute) Order-Web REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/search/" + genericAttribute,
				HttpMethod.GET, request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				ErrorDto errorDto = new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
				List<Object> errorlist = new ArrayList<Object>();
				errorlist.add(errorDto);
				return errorlist;
			} else {
				GenericOrderSearchDto[] orderDtoList = objectMapper.readValue(responseBody,
						GenericOrderSearchDto[].class);
				List<Object> objectList = new ArrayList<Object>();
				objectList.addAll(Arrays.asList(orderDtoList));
				return objectList;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Create Order based on provided JSON data for order creation
	 * 
	 * @return Order creation message. Else error message
	 */
	public Object createOrder(Orders order) {

		logger.info("Calling getGenericSearch (String genericAttribute) Order-Web REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<Orders> request = new HttpEntity<Orders>(order, headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/createorder", HttpMethod.POST, request,
				String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				ErrorDto errorDto = new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
				return errorDto;
			} else {
				OrderCreationDto orderDto = objectMapper.readValue(responseBody, OrderCreationDto.class);

				return orderDto;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
