package com.lego.microservices.services.web;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.dto.ErrorDto;

/**
 * Client controller, fetches Order info from the microservice via
 * {@link OrderWebService}.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class OrderWebController {

	@Autowired
	protected OrderWebService orderWebService;

	protected Logger logger = Logger.getLogger(OrderWebController.class.getName());

	public OrderWebController(OrderWebService orderWebService) {
		this.orderWebService = orderWebService;
	}
	
	/**
	 * Fetch list of all orders
	 * 
	 * @return
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/orders", produces = MediaType.APPLICATION_JSON)
	public List<Object> getAllOrders() {

		List<Object> orderDto = null;
		try {
			logger.info("OrderWeb-service getAllOrders() invoked");

			orderDto = orderWebService.getAllOrders();

			logger.info("OrderWeb-service getAllOrders() found");
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			List<Object> errorlist = new ArrayList<Object>();
			errorlist.add(errorDto);
			return errorlist;
		}

		return orderDto;
	}
	
	/**
	 * Fetch list of all orders filtered by Role Attribute name and value
	 * 
	 * @return
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/orders/{roleAttrName}/{roleAttrValue}", produces = MediaType.APPLICATION_JSON)
	public List<Object> getOrders(@PathVariable("roleAttrName") String roleAttrName,
			@PathVariable("roleAttrValue") String roleAttrValue) {

		List<Object> orderDto = null;
		try {
			logger.info("OrderWeb-service getOrders(String roleAttrName,String roleAttrValue) invoked");

			orderDto = orderWebService.getOrders(roleAttrName, roleAttrValue);
			logger.info("OrderWeb-service getOrders(String roleAttrName,String roleAttrValue) found");
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			List<Object> errorlist = new ArrayList<Object>();
			errorlist.add(errorDto);
			return errorlist;
		}

		return orderDto;
	}
	
	/**
	 * Fetch order details by order ID
	 * 
	 * @return
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/orders/{orderId}", produces = MediaType.APPLICATION_JSON)
	public Object getOrderById(@PathVariable("orderId") long orderId) {

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getOrderById(long orderId) invoked");

			orderDto = orderWebService.getOrderById(orderId);
			logger.info("OrderWeb-service getOrderById(long orderId) found");
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			return errorDto;
		}

		return orderDto;
	}
	
	/**
	 * Fetch list of all orders basis on orderId, mouldNo and artNo
	 * 
	 * @return List<Object>, if order found. Else error message
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/search/{genericAttribute}", produces = MediaType.APPLICATION_JSON)
	public Object getGenericSearch(@PathVariable("genericAttribute") String genericAttribute) {

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) invoked");

			orderDto = orderWebService.getGenericSearch(genericAttribute);
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) found");
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
			return errorDto;
		}

		return orderDto;
	}

	/**
	 * Create Order based on provided JSON data for order creation
	 * 
	 * @param order,
	 *            JSON object
	 * @return Order creation message. Else error message
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/createorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object createOrder(@RequestBody Orders order) {

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) invoked");

			orderDto = orderWebService.createOrder(order);
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) found");
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
			return errorDto;
		}

		return orderDto;
	}
}
