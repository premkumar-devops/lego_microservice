package com.lego.microservices.services.web;

import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.dto.ErrorDto;

/**
 * @author Suresh.Rupnar
 *
 */
/**
 * Client controller, fetches Roles info from the microservice via
 * {@link RolesWebService}.
 * 
 */
@RestController
public class RolesWebController {

	@Autowired
	protected RolesWebService rolesWebService;

	protected Logger logger = Logger.getLogger(RolesWebController.class.getName());
	
	/***
	 * Constructor
	 * @param RolesWebService
	 */
	public RolesWebController(RolesWebService RolesWebService) {
		this.rolesWebService = RolesWebService;
	}
	
	/**
	 * fetch the roles by role Id
	 * @param roleid
	 * @return roles
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/roles/{roleid}", produces = MediaType.APPLICATION_JSON)
	public Object getRoles(@PathVariable("roleid") int roleid) {

		if (StringUtils.isEmpty(roleid)) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_WrongRoleID"),Messages.getMessage("errorCodeMessage_WrongRoleID"));
			return errorDto;
		}

		Object rolesDto = null;
		try {
			logger.info("Roles web-service getRoles(int roleid) invoked: " + roleid);

			rolesDto = rolesWebService.getRoles(roleid);
			logger.info("Roles web-service getRoles(int roleid) found: " + roleid);
		} catch (HttpClientErrorException e) {

			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_WrongRoleID"),Messages.getMessage("errorCodeMessage_WrongRoleID"));
			return errorDto;
		}

		return rolesDto;
	}
	
	/**
	 * fetch roles by user Id
	 * @param userid
	 * @return roles
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/roles/user/{userid}", produces = MediaType.APPLICATION_JSON)
	public Object getRolesbyUser(@PathVariable("userid") String userid) {

		// if user ID and/or password are blank
		if (StringUtils.isEmpty(userid)) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),	Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			return errorDto;
		}

		Object usersdto = null;
		try {
			logger.info("Roles web-service getRolesByuserId(int roleid) invoked: " + userid);

			usersdto = rolesWebService.getRolesByuserId(userid);
			if (usersdto == null) {
				ErrorDto errorDto1 = new ErrorDto(Messages.getMessage("errorCode_NoUserFound"),Messages.getMessage("errorCodeMessage_NoUserFound"));
				return errorDto1;
			}

			logger.info("Roles web-service getRolesByuserId(int roleid) found: " + userid);
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_WrongRoleID"),Messages.getMessage("errorCodeMessage_WrongRoleID"));
			return errorDto;
		}

		return usersdto;
	}

}
