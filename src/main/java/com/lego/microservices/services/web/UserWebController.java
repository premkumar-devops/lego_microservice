/**
 * 
 */
package com.lego.microservices.services.web;

import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.dto.ErrorDto;

/**
 * Client controller, fetches User info from the microservice via
 * {@link UserWebService}.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class UserWebController {

	@Autowired
	protected UserWebService userWebService;

	protected Logger logger = Logger.getLogger(UserWebController.class.getName());

	public UserWebController(UserWebService userWebService) {
		this.userWebService = userWebService;
	}

	/**
	 * Validate user by given user Id and password
	 * 
	 * @param userID
	 * @param password
	 * @return user details, if success. Else error message
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/validate/{userID}/{password}", produces = MediaType.APPLICATION_JSON)
	public Object validate(@PathVariable("userID") String userID, @PathVariable("password") String password) {
		logger.info("userWeb-service validate() invoked: " + userID);
		// if user ID and/or password are blank
		if (StringUtils.isEmpty(userID) && StringUtils.isEmpty(password)) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			return errorDto;
		}

		Object userDto = null;
		try {
			userDto = userWebService.validateUser(userID, password);
			logger.info("userWeb-service validate() found: " + userID);
		} catch (HttpClientErrorException e) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("Invalid_UserName_Password_code"),
					Messages.getMessage("Invalid_UserName_Password_Message"));
			return errorDto;
		}

		return userDto;
	}
}
