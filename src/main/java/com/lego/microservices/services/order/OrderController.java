package com.lego.microservices.services.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lego.microservices.exceptions.NoOrderDetailsException;
import com.lego.microservices.persistence.Linedetails;
import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.Orderstatus;
import com.lego.microservices.persistence.dto.GenericOrderSearchDto;
import com.lego.microservices.persistence.dto.LinedetailsDto;
import com.lego.microservices.persistence.dto.OrderCreationDto;
import com.lego.microservices.persistence.dto.OrderDto;
import com.lego.microservices.persistence.dto.OrderSummaryDto;
import com.lego.microservices.persistence.dto.OrderstatusDto;
import com.lego.microservices.persistence.repository.OrderRepository;
import com.lego.microservices.persistence.repository.OrderStatusRepository;
import com.lego.microservices.persistence.repository.RolesRepository;

/**
 * A RESTFul controller for accessing user information.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
@Transactional
public class OrderController {

	@PersistenceContext
	private EntityManager em;

	protected Logger logger = Logger.getLogger(OrderController.class.getName());
	protected OrderRepository orderRepository;
	protected RolesRepository rolesRepository;
	protected OrderStatusRepository orderStatusRepository;
	private Orders order;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * Create an instance plugging in the respository of Users.
	 * 
	 * @param userRepository
	 *            An user repository implementation.
	 */
	@Autowired
	public OrderController(OrderRepository orderRepository, RolesRepository rolesRepository,
			OrderStatusRepository orderStatusRepository) {
		this.orderRepository = orderRepository;
		this.rolesRepository = rolesRepository;
		this.orderStatusRepository = orderStatusRepository;

		logger.info("OrderRepository and RolesRepository called");
	}

	@RequestMapping("/orders")
	public List<OrderSummaryDto> getAllOrders() {
		logger.info("order-service getAllOrders() invoked: ");
		List<Orders> orders = orderRepository.findAll();

		List<OrderSummaryDto> orderSummaryDtoList = new ArrayList<OrderSummaryDto>();
		OrderSummaryDto orderSummaryDto = null;
		for (Orders orders2 : orders) {
			orderSummaryDto = new OrderSummaryDto();
			orderSummaryDto.setOrderId(orders2.getOrderId());
			orderSummaryDto.setStatus(orders2.getPostatus());
			orderSummaryDto.setDescription(orders2.getStatusDescription());
			orderSummaryDtoList.add(orderSummaryDto);
		}

		if (orderSummaryDtoList.isEmpty())
			throw new NoOrderDetailsException("");
		return orderSummaryDtoList;
	}
	
	/**
	 * Fetch list of all orders filtered by Role Attribute name and value
	 * 
	 * @return
	 */
	@RequestMapping("/orders/{roleAttrName}/{roleAttrValue}")
	public List<OrderSummaryDto> getAllOrdersByAttribute(@PathVariable("roleAttrName") String roleAttrName,
			@PathVariable("roleAttrValue") String roleAttrValue) {

		logger.info("order-service getAllOrdersByAttribute(roleAttrName1,roleAttrName2) invoked: ");
		List<Orders> orders = new ArrayList<Orders>();
		if (roleAttrName.equalsIgnoreCase("postatus"))
			orders = orderRepository.findBypostatus(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("updatedBy"))
			orders = orderRepository.findByupdatedBy(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("createdBy"))
			orders = orderRepository.findBycreatedBy(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseCity"))
			orders = orderRepository.findBywarehouseCity(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("supplierAddress1"))
			orders = orderRepository.findBysupplierAddress1(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseAddress2"))
			orders = orderRepository.findBywarehouseAddress2(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseCountry"))
			orders = orderRepository.findBywarehouseCountry(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("supplierId"))
			orders = orderRepository.findBysupplierId(Integer.parseInt(roleAttrValue.toLowerCase()));
		else if (roleAttrName.equalsIgnoreCase("supplierCountry"))
			orders = orderRepository.findBysupplierCountry(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("brokerId"))
			orders = orderRepository.findBybrokerId(roleAttrValue);

		List<OrderSummaryDto> orderSummaryDtoList = new ArrayList<OrderSummaryDto>();
		OrderSummaryDto orderSummaryDto = null;
		for (Orders orders2 : orders) {
			orderSummaryDto = new OrderSummaryDto();
			orderSummaryDto.setOrderId(orders2.getOrderId());
			orderSummaryDto.setStatus(orders2.getPostatus());
			orderSummaryDtoList.add(orderSummaryDto);
		}

		if (orderSummaryDtoList.isEmpty())
			throw new NoOrderDetailsException("");
		return orderSummaryDtoList;
	}
	
	/**
	 * Fetch order details by order ID
	 * 
	 * @return
	 */
	@RequestMapping("/orders/{orderId}")
	public OrderDto getOrdersByID(@PathVariable("orderId") long orderId) {
		logger.info("order-service getOrdersByID() invoked: ");
		order = orderRepository.findByOrderId(orderId);

		OrderDto orderDto = new OrderDto();
		orderDto.setBrokerId(order.getBrokerId());
		orderDto.setCreatedBy(order.getCreatedBy());
		orderDto.setCreatedDate(dateFormat.format(order.getCreatedDate()));
		orderDto.setOrderId(order.getOrderId());
		orderDto.setPostatus(order.getPostatus());
		orderDto.setSupplierAddress1(order.getSupplierAddress1());
		orderDto.setSupplierAddress2(order.getSupplierAddress2());
		orderDto.setSupplierCity(order.getSupplierCity());
		orderDto.setSupplierCountry(order.getSupplierCountry());
		orderDto.setSupplierId(order.getSupplierId());
		orderDto.setUpdatedBy(order.getUpdatedBy());
		orderDto.setUpdatedDate(dateFormat.format(order.getUpdatedDate()));
		orderDto.setWarehouseAddress1(order.getWarehouseAddress1());
		orderDto.setWarehouseAddress2(order.getWarehouseAddress2());
		orderDto.setWarehouseCity(order.getWarehouseCity());
		orderDto.setWarehouseCountry(order.getWarehouseCountry());
		orderDto.setPoValue(order.getPoValue());
		orderDto.setStatusdescription(order.getStatusDescription());
		orderDto.setCurrency(order.getCurrency());

		LinedetailsDto linedetailsDto = null;
		OrderstatusDto orderstatuDto = null;
		Set<LinedetailsDto> linedetailsSet = new HashSet<LinedetailsDto>();
		Set<OrderstatusDto> orderstatusesSet = null;

		Set<Linedetails> linedetails = order.getLinedetailses();
		for (Linedetails linedetails1 : linedetails) {
			linedetailsDto = new LinedetailsDto();
			linedetailsDto.setArtNo(linedetails1.getArtNo());
			linedetailsDto.setCreatedAt(linedetails1.getCreatedBy());
			linedetailsDto.setLineId(linedetails1.getLineId());
			linedetailsDto.setMouldNo(linedetails1.getMouldNo());
			linedetailsDto.setQuantity(linedetails1.getQuantity());
			linedetailsDto.setStatus(linedetails1.getStatus());
			linedetailsDto.setUnitPrice(linedetails1.getUnitPrice());
			linedetailsDto.setLineDescription(linedetails1.getLineDescription());

			List<Orderstatus> orderstatus = orderStatusRepository.findbyOrderStatus(order.getOrderId(),
					linedetails1.getLineId());
			orderstatusesSet = new HashSet<OrderstatusDto>();
			for (Orderstatus orderstatus2 : orderstatus) {
				orderstatuDto = new OrderstatusDto();
				orderstatuDto.setId(orderstatus2.getId());
				orderstatuDto.setOrderId(orderstatus2.getOrders().getOrderId());
				orderstatuDto.setLineId(orderstatus2.getLinedetails().getLineId());
				orderstatuDto.setAttrName(orderstatus2.getAttrName());
				orderstatuDto.setAttrValue(orderstatus2.getAttrValue());
				orderstatuDto.setCreatedBy(orderstatus2.getCreatedBy());
				orderstatuDto.setCreatedDate(dateFormat.format(orderstatus2.getCreatedDate()));
				orderstatuDto.setDescription(orderstatus2.getDescription());
				orderstatuDto.setStatus(orderstatus2.getStatus());

				orderstatusesSet.add(orderstatuDto);
			}

			linedetailsDto.setOrderStatusesDto(orderstatusesSet);
			linedetailsSet.add(linedetailsDto);
		}
		orderDto.setLinedetailsesDto(linedetailsSet);

		return orderDto;
	}
	
	/**
	 * Fetch list of all orders basis on orderId, mouldNo and artNo
	 * 
	 * @return List<Object>, if order found. Else error message
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/search/{genericAttribute}")
	public List<GenericOrderSearchDto> getGenericSearch(@PathVariable("genericAttribute") String genericAttribute) {
		logger.info("order-service getGenericSearch() invoked: ");

		List<Object[]> resultList = em.createQuery(
				"Select distinct ord.orderId,ord.postatus, ord.statusDescription from Orders ord, Linedetails line where ord.orderId = line.orders.orderId and (ord.orderId like '%"
						+ genericAttribute + "%' or line.mouldNo like '%" + genericAttribute
						+ "%' or line.artNo like '%" + genericAttribute + "%')")
				.getResultList();

		if (resultList.isEmpty()) {
			throw new NoOrderDetailsException("");
		}

		List<GenericOrderSearchDto> genericOrderSearcchDtoList = new ArrayList<GenericOrderSearchDto>();
		GenericOrderSearchDto genericOrderSearcchDto = null;
		for (Object[] row : resultList) {
			genericOrderSearcchDto = new GenericOrderSearchDto();
			genericOrderSearcchDto.setOrderId(Long.parseLong(row[0].toString()));
			genericOrderSearcchDto.setStatus(row[1].toString());
			genericOrderSearcchDto.setDescription(row[2].toString());

			genericOrderSearcchDtoList.add(genericOrderSearcchDto);
		}

		return genericOrderSearcchDtoList;
	}
	
	/**
	 * Create Order based on provided JSON data for order creation
	 * 
	 * @param order,
	 *            JSON object
	 * @return Order creation message. Else error message
	 */
	@RequestMapping(method = RequestMethod.POST, path = "/createorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public OrderCreationDto createOrder(Orders order) {
		OrderCreationDto orderCreationDto = new OrderCreationDto();
		em.persist(order);
		List<Integer> lineId = new ArrayList<Integer>();
		Set<Linedetails> lindetails = order.getLinedetailses();
		for (Linedetails linedetails : lindetails) {
			linedetails.setOrders(order);
			em.persist(linedetails);
			lineId.add(linedetails.getLineId());
			for (Orderstatus orderstatus : linedetails.getOrderstatuses()) {
				orderstatus.setOrders(order);
				orderstatus.setLinedetails(linedetails);
				em.persist(orderstatus);
			}
		}
		em.flush();
		logger.info("order-service createOrder(Orders order) found: ");
		orderCreationDto.setOrderId(order.getOrderId());
		orderCreationDto.setLineId(lineId);
		
		return orderCreationDto;
	}

}
