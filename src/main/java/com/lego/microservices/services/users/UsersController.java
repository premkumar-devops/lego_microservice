/**
 * 
 */
package com.lego.microservices.services.users;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lego.microservices.exceptions.CannotPerformOperationException;
import com.lego.microservices.exceptions.InvalidHashException;
import com.lego.microservices.exceptions.PasswordMismatchException;
import com.lego.microservices.exceptions.UserNotFoundException;
import com.lego.microservices.persistence.Users;
import com.lego.microservices.persistence.repository.UserRepository;
import com.lego.microservices.services.util.PasswordUtil;

/**
 * A RESTFul controller for accessing user information.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class UsersController {

	protected Logger logger = Logger.getLogger(UsersController.class.getName());
	protected UserRepository userRepository;

	/**
	 * Create an instance plugging in the respository of Users.
	 * 
	 * @param userRepository
	 *            An user repository implementation.
	 */
	@Autowired
	public UsersController(UserRepository userRepository) {
		this.userRepository = userRepository;

		logger.info("UserRepository says system has " + userRepository.countUsers() + " users");
	}

	/**
	 * Validate user with the specified user Id and password.
	 * 
	 * @param userID
	 *            A varchar, user identifier.
	 * @param password
	 *            A varchar, user password.
	 * @return The user if found.
	 * @throws UserNotFoundException
	 *             If the user ID is not recognized.
	 */
	@RequestMapping("/users/validate/{userID}/{password}")
	public Users validate(@PathVariable("userID") String userID, @PathVariable("password") String password) {

		logger.info("users-service validate() invoked: " + userID);
		Users users = userRepository.findByUserId(userID);
		logger.info("users-service validate() found: " + users);

		if (users == null)
			throw new UserNotFoundException(userID," ");
		
		boolean flag;
		try {
			flag = PasswordUtil.verifyPassword(password.toCharArray(), users.getHash(), users.getSalt());
		} catch (CannotPerformOperationException e) {
			throw new CannotPerformOperationException("Cannot verify password");
		} catch (InvalidHashException e) {
			throw new InvalidHashException("Cannot verify password");
		}
		
		if (!flag)
			throw new PasswordMismatchException();

		return users;
	}
}
