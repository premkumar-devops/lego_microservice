drop table if exists Users ;
drop table if exists roles ;
drop table if exists ShipmentDetails;
drop table if exists OrderStatus;
drop table if exists LineDetails;
drop table if exists Orders;


CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(200) DEFAULT NULL,
  `AttrName` varchar(200) DEFAULT NULL,
  `AttrValue` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
);

CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(40) NOT NULL,
  `Salt` varchar(200) DEFAULT NULL,
  `Hash` varchar(200) DEFAULT NULL,
  `RoleID` int(11) DEFAULT NULL,
  `LastLoginTime` datetime DEFAULT NULL,
  `CreatedTime` datetime DEFAULT NULL,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`)
);

CREATE TABLE `orders` (
  `OrderID` bigint(20) NOT NULL AUTO_INCREMENT,
  `POStatus` varchar(200) DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `WarehouseCity` varchar(200) DEFAULT NULL,
  `SupplierAddress1` varchar(200) DEFAULT NULL,
  `SupplierAddress2` varchar(200) DEFAULT NULL,
  `WarehouseAddress1` varchar(200) DEFAULT NULL,
  `WarehouseAddress2` varchar(200) DEFAULT NULL,
  `WarehouseCountry` varchar(200) DEFAULT NULL,
  `SupplierId` int(11) DEFAULT NULL,
  `SupplierCity` varchar(200) DEFAULT NULL,
  `SupplierCountry` varchar(200) DEFAULT NULL,
  `BrokerId` varchar(200) DEFAULT NULL,
  `StatusDescription` varchar(200) DEFAULT NULL,
  `PoValue` double DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Currency` varchar(45) DEFAULT NULL,  
  PRIMARY KEY (`OrderID`)
);


CREATE TABLE `linedetails` (
  `LineID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` bigint(20) DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `MouldNo` varchar(200) DEFAULT NULL,
  `ArtNo` int(11) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Status` varchar(200) DEFAULT NULL,
  `LineDescription` varchar(200) DEFAULT NULL,
  `UnitPrice` double DEFAULT NULL,  
  PRIMARY KEY (`LineID`),
  KEY `OrderID` (`OrderID`),
  CONSTRAINT `linedetails_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`)
);

CREATE TABLE `orderstatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` bigint(20) DEFAULT NULL,
  `LineID` int(11) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `AttrName` varchar(200) DEFAULT NULL,
  `AttrValue` varchar(200) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `OrderID` (`OrderID`),
  KEY `LineID` (`LineID`),
  CONSTRAINT `orderstatus_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`),
  CONSTRAINT `orderstatus_ibfk_2` FOREIGN KEY (`LineID`) REFERENCES `linedetails` (`LineID`)
);

CREATE TABLE `shipmentdetails` (
  `TrackingID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` bigint(20) DEFAULT NULL,
  `LineID` int(11) DEFAULT NULL,
  `ShippingAgency` varchar(200) DEFAULT NULL,
  `LastStatus` varchar(200) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`TrackingID`),
  KEY `OrderID` (`OrderID`),
  KEY `LineID` (`LineID`),
  CONSTRAINT `shipmentdetails_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`),
  CONSTRAINT `shipmentdetails_ibfk_2` FOREIGN KEY (`LineID`) REFERENCES `linedetails` (`LineID`)
);
