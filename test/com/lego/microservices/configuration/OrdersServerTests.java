package com.lego.microservices.configuration;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lego.microservices.services.order.OrderServer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OrderServer.class)
public class OrdersServerTests {

	@Test
	public void contextLoads() {
	}

}
