# ROLES TABLE INSERT VALUES
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(1,"LegoRep", 'repID', 'rep3@lego.com');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(2,"Supplier", 'supplierID', '18548');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(3,"CustomsBroker", 'customsbroker', 'cus1');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(4,"WarehousEmp", 'warehouse_loc', 'billund');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(5,"Admin",'', '');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(6,"WarehousEmp", 'warehouse_loc', 'hungary');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(7,"WarehousEmp", 'warehouse_loc', 'Mexico');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(8,"LegoRep", 'repID', 'rep2@lego.com');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(9,"LegoRep", 'repID', 'rep3@lego.com');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(10,"Supplier", 'supplierID', '121749');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(11,"Supplier", 'supplierID', 'sup3');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(12,"CustomsBroker", 'customsbroker', 'cus2');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(13,"CustomsBroker", 'customsbroker', 'cus3');
#**********************************

#USERS TABLE INSERT VALUES
#*************************

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES
	("sivap","213123","sdfsdffsdfsdds",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("premk","2131321","sfasfadsadasd",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"system");

INSERT INTO USERS
 	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("navanee","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
 	VALUES 
	("vamsi","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	 VALUES 
	("karuna","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("admin","21312","q34242323234",5,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES
	("navanee1","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("vamsi1","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("karuna1","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("premk1","2131321","sfasfadsadasd",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("navanee2","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("vamsi2","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("karuna2","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");
#**********************************

UPDATE users SET salt ="hz4bWIjfaveTwdEUppbHhonRYSPMYN3x", hash="sCcq7vf+gqe/BWC9fwQ+T8zl" where userid = "sivap";
UPDATE users SET salt ="U8kp66nGL6CuLwcHK3ofe5yK6eepBTGs" , hash="EYHsEAkusa1Q3QmWFHD3WF98" where userid = "premk";
UPDATE users SET salt ="FCtEEzH+FwFWIkFLdqA2glJEzIlK/R4b" , hash="jFotOl8vpdP/4vkcwcsGfVDZ" where userid = "navanee";
UPDATE users SET salt ="0OcpdN8R/iNGz9XgPy5ugEyeAIBUz/Wm" , hash="MbQhR7/T76nLCX82MOzv6pAl" where userid = "vamsi";
UPDATE users SET salt ="z2chksoDf/I31ydwN1VzeGcDssJc00m1" , hash="OrfHGfJaf3UoXuPQ6H/EkBBJ" where userid = "karuna";
UPDATE users SET salt ="3MS4UzGb5whfld7Oqpm/FyV0pQobz0Xv" , hash="ohZ28QZzRDpNmmPYbcAtZhu2" where userid = "admin";
UPDATE users SET salt ="aITmCHJBBWGJvkyJ1ZlJk8HJpKAawZHi" , hash="aUcglOncpb4gM1gGRvrLXrkh" where userid = "navanee1";
UPDATE users SET salt ="xWC0UZNzQ+ge7B2ap9AZ9Grh85KhNKHe" , hash="JW5SOM+sb1i4RfYqabIdCyNq" where userid = "vamsi1";
UPDATE users SET salt ="3pBcWkaGD/+Ys8v7XtHoZKHr0JtnskD3" , hash="WJDid2Oyo1sqBY1dyjNKVjkE" where userid = "karuna1";
UPDATE users SET salt ="Yxj+4lbZ8O0A41qZ4UE36JcpEDA1+kym" , hash="vJd9dIwQ6q0m3n+fJLJlVQpk" where userid = "premk1";
UPDATE users SET salt ="iQc6wiPQ1IMQwAj31cy8G9wrVhHoNL0k" , hash="iGWIgL31KonfZliX8d6nyKc4" where userid = "navanee2";
UPDATE users SET salt ="MIJHnXubLflwNqr6KBLWak5Yq/CPXycv" , hash="NhPZh3p1nnZuRl6AYE/frjSE" where userid = "vamsi2";
UPDATE users SET salt ="rtT9XlNARH/5B15cDMmfj7M/lq7+2FYR" , hash="30GuuO85XJluvsRViZ70d87i" where userid = "karuna2";


#******************************
#Scenario 1
#*********************


INSERT INTO ORDERS(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity ,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES	(4501081977,"CREATED",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund",
"Denmark",'2016-11-28 00:00:00',"rep1@lego.com","rep1@lego.com",'2016-11-28 00:00:00',"");

INSERT INTO LineDetails
	(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081977, 10, '596X44x637', 5737311, "Billund", 4 ,"CREATED");

INSERT INTO LineDetails
	(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081977, 20, '600X455X37', 5737312, "Billund", 4, "CREATED");

INSERT INTO ORDERSTATUS
	(OrderID,LineID,Status,CreatedBy,CreatedDate)
	VALUES
	(4501081977, 10, "CREATED", "rep1@lego.com",'2016-11-28 00:00:00');

INSERT INTO ORDERSTATUS
	(OrderID,LineID,Status,CreatedBy,CreatedDate)
	VALUES
	(4501081977, 20, "CREATED", "rep1@lego.com",'2016-11-28 00:00:00');


#Scenario 2
#*********************

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081988,"CREATED",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep1@lego.com",'2016-11-22 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079888,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-22 00:00:00',"");

INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081988, 11, '596X44x637', 5737311, "Billund", 4 ,"CREATED");

INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081988, 21, '600X455X37', 573731,"Billund",4, "CREATED");

INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501079888, 12, '4817007', 4817007, "Billund",4, "CREATED");

INSERT INTO ORDERSTATUS
	(OrderID,LineID,Status,CreatedBy,CreatedDate)
	VALUES
	(4501081988, 11, "CREATED", "rep1@lego.com",'2016-11-28 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081988, 21, "CREATED", "rep1@lego.com",'2016-11-28 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079888, 12, "CREATED", "rep2@lego.com",'2016-11-28 00:00:00');


#Scenario 3
#************************


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081777,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",'2016-11-22 00:00:00',"rep1@lego.com","rep1@lego.com",'2016-11-22 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079666,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-22 00:00:00',"");



INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081777, 15, '596X44x637', 5737311, "Billund", 4 ,"CREATED");

INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501081777, 25, '600X455X37', 573731,"Billund",4, "CREATED");

INSERT INTO LineDetails
	(OrderID,LineID,MouldNo, ArtNo, CreatedAt, Quantity, Status)
	VALUES
	(4501079666, 32, '4817007', 4817007, "Billund",4, "CREATED");
    
    

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081777, 15, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081777, 25, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079666, 32, "CREATED", "rep2@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081777, 15, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081777, 25, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');



#Scenario 4
#******************


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081775,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep1@lego.com",'2016-11-22 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079665,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-22 00:00:00',"");

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079555,"CREATED",121778,"Addr1","Add2","HongKong","HongKong","Addr1","Addr2","Mexico","Mexico",
'2016-11-25 00:00:00',"rep3@lego.com","rep3@lego.com",'2016-11-22 00:00:00',"cus3");



INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081775 ,16, '596X44x637', 5737311, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081775, 17, '600X455X37', 5737312, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079665, 19, '4817007', 4817007, "Billund", 4, "CREATED");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079555, 18, '596X44x638', 4817008, "Mexico", 4, "CREATED");



INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081775, 16, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081775, 17, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079665, 19, "CREATED", "rep2@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081775, 16, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081775, 17, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079555, 18, "CREATED", "rep3@lego.com",'2016-11-25 00:00:00');


#*****************
#Scenario 5

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501082345,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep1@lego.com",'2016-11-22 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501073452,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-22 00:00:00',"");

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501078765,"RECEIVEDBYSUPPLIER",121778,"Addr1","Add2","HongKong","HongKong","Addr1","Addr2","Mexico","Mexico",
'2016-11-25 00:00:00',"rep3@lego.com","rep3@lego.com",'2016-11-22 00:00:00',"cus3");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501082345, 51, '596X44x637', 5737311, "Billund", 4, "RECEIVEDBYSUPPLIER");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501082345, 52, '600X455X37', 5737312, "Billund", 4, "RECEIVEDBYSUPPLIER");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501073452, 53, '4817007', 4817007, "Billund", 4, "CREATED");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501078765, 54, '596X44x638', 4817008, "Mexico", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501082345, 51, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501082345, 52, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501073452, 53, "CREATED", "rep2@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501082345, 51, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501082345, 52, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501078765, 54, "CREATED", "rep3@lego.com",'2016-11-25 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501078765, 54, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-26 00:00:00');


#Scenario 6
#**********************

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081987,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079985,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-23 00:00:00',"");

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079983,"SHIPPED",121778,"Addr1","Add2","HongKong","HongKong","Addr1","Addr2","Mexico","Mexico",
'2016-11-25 00:00:00',"rep3@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"cus3");



INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081987, 61, '596X44x637', 5737311, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081987, 62, '600X455X37', 5737312, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079985, 65, '4817007', 4817007, "Billund", 4, "CREATED");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079983, 66, '596X44x638', 4817008, "Mexico", 4, "SHIPPED");


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081987, 62, "CREATED", "rep1@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079985, 65, "CREATED", "rep2@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081987, 61, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081987, 62, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079983, 66, "CREATED", "rep3@lego.com",'2016-11-25 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079983, 66, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-26 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079983, 66, "SHIPPED", "Supplieruserid",'2016-11-27 00:00:00');




INSERT INTO SHIPMENTDETAILS
(ORDERID,LINEID,SHIPPINGAGENCY,TRACKINGID,LASTSTATUS,UPDATEDDATE)
VALUES
(4501079983, 66, "DHL", 12312345,"NEW",'2016-11-27 00:00:00');



#***************************

#Scenario 7

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081456,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079465,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-23 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-23 00:00:00',"");


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079476,"RECEIVEDBYBROKER",121778,"Addr1","Add2","HongKong","HongKong","Addr1","Addr2","Mexico","Mexico",
'2016-11-25 00:00:00',"rep3@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"cus3");



INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081456, 35, '596X44x637', 5737311, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081456, 37, '600X455X37', 5737312, "Billund", 4, "RECEIVEDBYSUPPLIER");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079465, 39, '4817007', 4817007, "Billund", 4, "CREATED");


INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079476, 40, '596X44x638', 4817008, "Mexico", 4, "RECEIVEDBYBROKER");



INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081456, 35, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');


INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081456, 37, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079465, 39, "CREATED", "rep2@lego.com",'2016-11-23 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081456, 35, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-24 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081456, 37, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-28 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079476, 40, "CREATED", "rep3@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079476, 40, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079476, 40, "SHIPPED", "Supplieruserid",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079476, 40, "RECEIVEDBYBROKER", "Customerbrokeruserid",'2016-11-22 00:00:00');




INSERT INTO SHIPMENTDETAILS
(ORDERID,LINEID,SHIPPINGAGENCY,TRACKINGID,LASTSTATUS,UpdatedDate)
VALUES
(4501079476, 40, "DHL", 12312346,	"DELIVERED", '2016-11-22 00:00:00');


#**************************
#Scenario 8


INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501081111,"RECEIVEDBYSUPPLIER",18548,"Addr1","Add2","Suzhou","China","Addr1","Addr2","Billund","Denmark",
'2016-11-22 00:00:00',"rep1@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"");

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079222,"CREATED",121749,"Addr1","Add2","Geneva","Switzerland","Addr1","Addr2","Budapest","Hungary",
'2016-11-22 00:00:00',"rep2@lego.com","rep2@lego.com",'2016-11-23 00:00:00',"");

INSERT INTO ORDERS 
(OrderID,POStatus,SupplierID,SupplierAddress1,SupplierAddress2,SupplierCity,SupplierCountry,WareHouseAddress1,WareHouseAddress2,WareHouseCity,WareHouseCountry,CreatedDate,CreatedBy,UpdatedBy,UpdatedDate,BrokerID) 
VALUES(4501079333,"RECEIVEDATAWAREHOUSE",121778,"Addr1","Add2","HongKong","HongKong","Addr1","Addr2","Mexico","Mexico",
'2016-11-22 00:00:00',"rep3@lego.com","rep3@lego.com",'2016-11-23 00:00:00',"cus3");




INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081111, 71, '596X44x637', 5737311, "Billund", 4, "RECEIVEDBYSUPPLIER");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501081111, 73, '600X455X37', 5737312, "Billund", 4 ,"RECEIVEDBYSUPPLIER");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079222, 75, '4817007', 4817007, "Billund", 4, "CREATED");

INSERT INTO LineDetails
(OrderID, LineID, MouldNo, ArtNo, CreatedAt, Quantity, Status)
VALUES
(4501079333, 77, '596X44x638', 4817008, "Mexico", 4, "RECEIVEDATAWAREHOUSE");



INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081111, 71, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081111, 73, "CREATED", "rep1@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079222, 75, "CREATED", "rep2@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081111, 71, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-28 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501081111, 73, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-28 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079333, 77, "CREATED", "rep3@lego.com",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079333, 77, "RECEIVEDBYSUPPLIER", "Supplieruserid",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079333, 77, "SHIPPED", "Supplieruserid",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079333, 77, "RECEIVEDBYBROKER", "Customerbrokeruserid",'2016-11-22 00:00:00');

INSERT INTO ORDERSTATUS
(OrderID,LineID,Status,CreatedBy,CreatedDate)
VALUES
(4501079333, 77, "RECEIVEDATAWAREHOUSE", "Warehouseuserid",'2016-11-22 00:00:00');


INSERT INTO SHIPMENTDETAILS
(ORDERID,LINEID,SHIPPINGAGENCY,TRACKINGID,LASTSTATUS,UpdatedDate)
VALUES
(4501079333, 77, "DHL", 12312347,	"DELIVERED", '2016-11-22 00:00:00');