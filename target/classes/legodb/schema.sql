drop table if exists Users if exists;
drop table if exists roles if exists;
drop table if exists ShipmentDetails if exists;
drop table if exists OrderStatus if exists;
drop table if exists LineDetails if exists;
drop table if exists Orders if exists;

create TABLE Roles(
		RoleID INT NOT NULL AUTO_INCREMENT,
		RoleName VARCHAR(200),
		AttrName VARCHAR(200),
		AttrValue VARCHAR(200),
		primary key (RoleID));
		
create TABLE Users( 
		ID INT NOT NULL AUTO_INCREMENT,
		UserID VARCHAR(40) NOT NULL,
		Salt VARCHAR(200),
		Hash VARCHAR(200),
		RoleID INT,
		LastLoginTime DATETIME,
		CreatedTime DATETIME,
		CreatedBy VARCHAR(100),
		UpdatedTime DATETIME,
		UpdatedBy VARCHAR(200),
		FOREIGN KEY (RoleID) REFERENCES Roles(RoleID),
		primary key (ID));

create TABLE Orders(
		OrderID INT NOT NULL AUTO_INCREMENT,
		POStatus VARCHAR(200),
		CreatedAt VARCHAR(200),
		CreatedBy VARCHAR(200),
		CreatedDate DATETIME,
		SupplierName VARCHAR(200),
		SupplierAddress VARCHAR(200),
		WarehouseAddress VARCHAR(200),
		POValue INT,
		Quantity INT,
		PartNo INT,
		MouldNo INT,
		primary key (OrderID));

create TABLE LineDetails(
		LineID INT NOT NULL AUTO_INCREMENT,
		OrderID INT,
		CreatedDate DATETIME,
		CreatedBy VARCHAR(200),
		primary key (LineID),
		FOREIGN KEY (OrderID) REFERENCES Orders(OrderID));       
        
create TABLE OrderStatus(
		OrderID INT,
		LineID INT,
		Status VARCHAR(100),
		CreatedBy VARCHAR(200),
		CreatedDate DATETIME,
		AttrName VARCHAR(200),
		AttrValue VARCHAR(200) ,
		FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
		FOREIGN KEY (LineID) REFERENCES LineDetails(LineID));

create TABLE ShipmentDetails(
		TrackingID INT NOT NULL AUTO_INCREMENT,
		OrderID INT,
		LineID INT,
		ShippingAgency VARCHAR(200),
		LastStatus VARCHAR(200),
		UpdatedAt DATETIME,
		primary key (TrackingID),
		FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
		FOREIGN KEY (LineID) REFERENCES LineDetails(LineID));
